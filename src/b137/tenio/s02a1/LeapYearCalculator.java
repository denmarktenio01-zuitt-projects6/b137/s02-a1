package b137.tenio.s02a1;

import java.util.Scanner;

public class LeapYearCalculator {
    public static void main(String[] args) {
        Scanner appScanner = new Scanner(System.in);

        System.out.println("What's your first name?");

        String fName = appScanner.nextLine();
        System.out.println("Hello " + fName + "!");

        // Activity: Check if a year is a leap year or not.



        System.out.println("Welcome to Leap Year Checker!");
        System.out.println("Enter the year:");
        int year = appScanner.nextInt();

        if (year % 4 == 0) {
            if (year % 100 == 0) {
                if(year % 400 == 0) {
                    System.out.println(year + " is a leap year.");

                } else {
                    System.out.println(year + " is not a leap year.");
                }
            } else {
                System.out.println(year + " is a leap year.");
            }
        } else {
            System.out.println(year + " is not a leap year.");
        }

    }
}
